export class WebGL{
    constructor(glctx, vertexShaderSource, fragmentShaderSource, gameCanvasWidth, gameCanvasHeight){
        this.gl = glctx;
        this.vertexShaderSource = vertexShaderSource;
        this.fragmentShaderSource = fragmentShaderSource;
        this.viewportWidth = gameCanvasWidth;
        this.viewportHeight = gameCanvasHeight;
        this.gl.viewport(0, 0, this.viewportWidth, this.viewportHeight);
        this.background = {
            "R" : 0.1,
            "G" : 0.1,
            "B" : 0.1,
            "A" : 1.0
        };
        this.color = {
            "R" : 1.0,
            "G" : 1.0,
            "B" : 1.0,
            "A" : 1.0           
        }; // white
    }
    
    getRectVertices(x, y, width, height) {
        let vertices = []; 

        vertices.push(x);
        vertices.push(y);

        vertices.push(x);
        vertices.push(y + height);

        vertices.push(x + width);
        vertices.push(y);

        vertices.push(x);
        vertices.push(y + height);

        vertices.push(x + width);
        vertices.push(y + height);

        vertices.push(x + width);
        vertices.push(y);
        
        return vertices;
    }

    drawRect(x, y, width, height, color = this.color) {
        //console.log(`[INFO]WebGL/drawRect: x: ${x}, y: ${y}, width: ${width}, height: ${height}, color:`, color);
        let vertices = this.getRectVertices(x, y, width, height);
        color = new Array((1.0/255)*color.R, (1.0/255)*color.G, (1.0/255)*color.B, (1.0/255)*color.A);

    
        function createShader(gl, type, source) {
            let shader = gl.createShader(type);
            gl.shaderSource(shader, source);
            gl.compileShader(shader);
            let success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
            if (success) {
              return shader;
            }
           
            console.log(gl.getShaderInfoLog(shader));
            gl.deleteShader(shader);
        }
        
        let vertexShader = createShader(this.gl, this.gl.VERTEX_SHADER, this.vertexShaderSource);
        let fragmentShader = createShader(this.gl, this.gl.FRAGMENT_SHADER, this.fragmentShaderSource);
        
        function createProgram(gl, vertexShader, fragmentShader) {
            let program = gl.createProgram();
            gl.attachShader(program, vertexShader);
            gl.attachShader(program, fragmentShader);
            gl.linkProgram(program);
            let success = gl.getProgramParameter(program, gl.LINK_STATUS);
            if (success) {
              return program;
            }
    
            console.log(gl.getProgramInfoLog(program));
            gl.deleteProgram(program);
        }   
        let program = createProgram(this.gl, vertexShader, fragmentShader);
        this.gl.useProgram(program);   
    
        let resolutionUniformLocation = this.gl.getUniformLocation(program,"u_resolution");
        let resolution = [
            this.viewportWidth, this.viewportHeight
        ];
        this.gl.uniform2fv(resolutionUniformLocation, resolution);  
    
        let positionAttributeLocation = this.gl.getAttribLocation(program, "a_position");
        let positionBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, positionBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(vertices), this.gl.STATIC_DRAW);
        this.gl.enableVertexAttribArray(positionAttributeLocation);

        let colorUniformLocation = this.gl.getUniformLocation(program, "u_color");
        this.gl.uniform4fv(colorUniformLocation, color);
        
        // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
        let size = 2;          // 2 components per iteration
        let type = this.gl.FLOAT;   // the data is 32bit floats
        let normalize = false; // don't normalize the data
        let stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
        let offset = 0;        // start at the beginning of the buffer
        this.gl.vertexAttribPointer(positionAttributeLocation, size, type, normalize, stride, offset);
    
        let primitiveType = this.gl.TRIANGLES;
        offset = 0;
        let count = vertices.length/2;
        this.gl.drawArrays(primitiveType, offset, count);     
    }    

    cls(){
        this.gl.clearColor(this.background.R, this.background.G, this.background.B, this.background.A);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    }

    setColor(color){
        this.color = color;
    }

    getColor(){
        return this.color;
    }
} 
