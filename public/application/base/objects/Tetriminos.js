export const Tetriminos = 
{
        "I": 
        {
            name : "I",
            color : {R: 0, G: 240, B: 240, A: 255},
            model :
                {
                N: [
                    0,0,0,0,
                    1,1,1,1,
                    0,0,0,0,
                    0,0,0,0
                ],
                E: [
                    0,0,1,0,
                    0,0,1,0,
                    0,0,1,0,
                    0,0,1,0
                ],
                S: [
                    0,0,0,0,
                    0,0,0,0,
                    1,1,1,1,
                    0,0,0,0
                ],
                W: [
                    0,1,0,0,
                    0,1,0,0,
                    0,1,0,0,
                    0,1,0,0
                ]
            },
            rotation: "N",
            width: 4,
            height: 4,
            x: 3,
            y: 20
        },
        "O": 
        {
            name : "O",
            color : {R: 231, G: 231, B: 0, A: 255},
            model :
                {
                N: [
                    0,0,1,1,
                    0,0,1,1,
                    0,0,0,0,
                    0,0,0,0
                ],
                E: [
                    0,0,1,1,
                    0,0,1,1,
                    0,0,0,0,
                    0,0,0,0
                ],
                S: [
                    0,0,1,1,
                    0,0,1,1,
                    0,0,0,0,
                    0,0,0,0
                ],
                W: [
                    0,0,1,1,
                    0,0,1,1,
                    0,0,0,0,
                    0,0,0,0
                ]
            },
            rotation: "N",
            width: 4,
            height: 4,
            x: 3,
            y: 20
        },
        "T": 
        {
            name : "T",
            color : {R: 161, G: 0, B: 240, A: 255},
            model :
                {
                N: [
                    0,1,0,
                    1,1,1,
                    0,0,0
                ],
                E: [
                    0,1,0,
                    0,1,1,
                    0,1,0
                ],
                S: [
                    0,0,0,
                    1,1,1,
                    0,1,0
                ],
                W: [
                    0,1,0,
                    1,1,0,
                    0,1,0
                ]
            },
            rotation: "N",
            width: 3,
            height: 3,
            x: 4,
            y: 20
        },
        "S": 
        {
            name : "S",
            color : {R: 71, G: 247, B: 71, A: 255},
            model :
                {
                N: [
                    0,1,1,
                    1,1,0,
                    0,0,0
                ],
                E: [
                    0,1,0,
                    0,1,1,
                    0,0,1
                ],
                S: [
                    0,0,0,
                    0,1,1,
                    1,1,0
                ],
                W: [
                    1,0,0,
                    1,1,0,
                    0,1,0
                ],
            },
            rotation: "N",
            width: 3,
            height: 3,
            x: 4,
            y: 20
        },
        "Z": 
        {
            name : "Z",
            color : {R: 240, G: 0, B: 0, A: 255},
            model :
                {
                N: [
                    1,1,0,
                    0,1,1,
                    0,0,0
                ],
                E: [
                    0,0,1,
                    0,1,1,
                    0,1,0
                ],
                S: [
                    0,0,0,
                    1,1,0,
                    0,1,1
                ],
                W: [
                    0,1,0,
                    1,1,0,
                    1,0,0
                ]
            },
            rotation: "N",
            width: 3,
            height: 3,
            x: 4,
            y: 20
        },
        "J": 
        {
            name : "J",
            color : {R: 0, G: 0, B: 239, A: 255},
            model :
                {
                N: [
                    1,0,0,
                    1,1,1,
                    0,0,0
                ],
                E: [
                    0,1,1,
                    0,1,0,
                    0,1,0
                ],
                S: [
                    0,0,0,
                    1,1,1,
                    0,0,1
                ],
                W: [
                    0,1,0,
                    0,1,0,
                    1,1,0
                ]
            },
            rotation: "N",
            width: 3,
            height: 3,
            x: 4,
            y: 20
        },
        "L": 
        {
            name : "L",
            color : {R: 250, G: 201, B: 101, A: 255},
            model :
                {
                N: [
                    0,0,1,
                    1,1,1,
                    0,0,0
                ],
                E: [
                    0,1,0,
                    0,1,0,
                    0,1,1
                ],
                S: [
                    0,0,0,
                    1,1,1,
                    1,0,0
                ],
                W: [
                    1,1,0,
                    0,1,0,
                    0,1,0
                ]
            },
            rotation: "N",
            width: 3,
            height: 3,
            x: 4,
            y: 20

        }
}