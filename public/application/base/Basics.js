import { Tetrimino } from "./objects/Tetrimino.js";

export class Basics {
  static getPageWidth() {
    return Math.max(
      document.body.scrollWidth,
      document.documentElement.scrollWidth,
      document.body.offsetWidth,
      document.documentElement.offsetWidth,
      document.documentElement.clientWidth
    );
  }
    
  static gePagetHeight() {
    return Math.max(
      document.body.scrollHeight,
      document.documentElement.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.offsetHeight,
      document.documentElement.clientHeight
    );
  }

  static addDebugTetriminos(field){
    for(var i = 0; i <=5; i++) {
        let temp;
        switch(i) {
            case 0:
                temp = new Tetrimino("O");
                temp.x = 4;
                temp.y = 36;
                break;
            case 1:
                temp = new Tetrimino("T");
                temp.x = 5;
                temp.y = 34;
                break;
            case 2:
                temp = new Tetrimino("S");
                temp.x = 7;
                temp.y = 37;
                break;
            case 3:
                temp = new Tetrimino("Z");
                temp.x = 4;
                temp.y = 37;
                break;
            case 4:
                temp = new Tetrimino("J");
                temp.x = 4;
                temp.y = 38;
                break;
            case 5:
                temp = new Tetrimino("L");
                temp.x = 7;
                temp.y = 38;
                break;
        }
        field.addTetrimino(temp)
    }
  }
  
  static sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  static  pad(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }
}