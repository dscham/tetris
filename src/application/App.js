import * as CFG from "./Cfg.js";
import {Tetrimino} from "./base/objects/Tetrimino.js";
import {WebGL} from "./webgl/WebGL.js";
import {Tetris} from "./base/objects/Tetris.js";
import {Renderer} from "./base/Renderer.js";
import {Basics} from "./base/Basics.js";
import {Tetriminos} from "./base/objects/Tetriminos.js";

// Constant
const fieldWidth = CFG.FIELD_WIDTH;
const fieldHeight = CFG.FIELD_HEIGHT;
const visibleFieldHeight = 20;

// DOM
const mainCanvas = document.querySelector("#main-canvas");
const holdCanvas = document.querySelector("#hold-canvas");
const nextCanvas = document.querySelector("#next-canvas");
const scoreDisplay = document.querySelector("#scoreboard-score");
const scoreTime = document.querySelector("#scoreboard-time");
const scoreLines = document.querySelector("#scoreboard-lines");
const scoreLevel = document.querySelector("#scoreboard-level");
const scoreGoal = document.querySelector("#scoreboard-goal");
const scoreTetrises = document.querySelector("#scoreboard-tetrises");
const scoreTpm = document.querySelector("#scoreboard-tpm");
const scoreLpm = document.querySelector("#scoreboard-lpm");
const pageTitle = document.querySelector("#page-title");
const pauseButtonContent = document.querySelector("#pause-button").cloneNode(true);
const pauseScreenContent = document.querySelector("#pause-screen").cloneNode(true);
document.querySelector("#pause-screen").remove();


// Shader
const vertexShaderSource = document.getElementById("2d-vertex-shader").text;
const fragmentShaderSource = document.getElementById("2d-fragment-shader").text;

// Main GFX
const mainGl = mainCanvas.getContext("webgl2");
const mainCanvasHeight = quantize(mainCanvas.getBoundingClientRect()["height"], visibleFieldHeight);
const mainCanvasWidth = aspectRatioWidth(mainCanvasHeight);
const mainMinosize = mainCanvasWidth / fieldWidth;
const mainWebgl = WebGL.getInstance(mainGl, vertexShaderSource, fragmentShaderSource, mainCanvasWidth, mainCanvasHeight);
const mainRenderer = Renderer.getInstance(mainWebgl, mainMinosize);

// Current tetrimino GFX
const holdGl = holdCanvas.getContext("webgl2");
const holdCanvasWidth = quantize(holdCanvas.getBoundingClientRect()["width"], 6);
const holdCanvasHeight = holdCanvasWidth;
const holdMinoSize = holdCanvasWidth / 6;
const holdWebgl = WebGL.getInstance(holdGl, vertexShaderSource, fragmentShaderSource, holdCanvasWidth, holdCanvasHeight);
const holdRenderer = Renderer.getInstance(holdWebgl, holdMinoSize);

// Next tetrimino GFX
const nextGl = nextCanvas.getContext("webgl2");
const nextCanvasWidth = quantize(nextCanvas.getBoundingClientRect()["width"], 6);
const nextCanvasHeight = nextCanvasWidth * 3;
const nextMinoSize = nextCanvasWidth / 6;
const nextWebgl = WebGL.getInstance(nextGl, vertexShaderSource, fragmentShaderSource, nextCanvasWidth, nextCanvasHeight);
const nextRenderer = Renderer.getInstance(nextWebgl, nextMinoSize);

// Var
let countingField = new Tetris(fieldWidth, fieldHeight);
let nextTetriminos = generateTetriminos(7);
let currentTetrimino = undefined;
let gameRunning = false;
let fallSpeed = 0.8;
let event = undefined;
let lastUpdate = 0;
let lastDraw = 0;
let lastMove = 0;
let lastX = 0;
let lastY = 0;
let touchStartPosition = {};
let startTime = 0;
let dropping = false;
let countedFPS = 100;
let countedTPS = 100;
let loopTime = 100;
let lastLoop = 100;

function main() {
    console.log(`[${CFG.LOGLEVEL.INFO}]${CFG.APPNAME}/main: Started!`);
    setDOMCanvasDimensions(mainCanvas, mainCanvasWidth, mainCanvasHeight);
    setDOMCanvasDimensions(holdCanvas, holdCanvasWidth, holdCanvasHeight);
    setDOMCanvasDimensions(nextCanvas, nextCanvasWidth, nextCanvasHeight);

    //Basics.addDebugTetriminos(countingField);

    gameRunning = true;
    startTime = Date.now();
    requestAnimationFrame(loop);
}

function size(object) {
    return object.keys.length;
}

function generateTetriminos(count){
    let tetriminos = [];
    for(let counter = 0; counter < count; counter++) {
        tetriminos.push(generateTetrimino());
    }
    return tetriminos;
}

function generateTetrimino() {
    let letters = ["I", "O", "S", "Z", "T", "J", "L"];
    let rand = letters[Math.floor(Math.random() *Math.floor(letters.length))];
    return new Tetrimino(rand);
}

function loop() {
    let timestamp = Date.now();
    

    if (gameRunning) {
        update();
    }
    draw();

    loopTime = timestamp - lastLoop;
    document.title = `JSTris [fps: ${countedFPS}, tps: ${countedTPS}, ms/l: ${loopTime}]`;
    lastLoop = Date.now();

    requestAnimationFrame(loop);
}

function millisConverter(millis) {
    return {
            hours: Math.floor((millis /3600000) %24),
            mins: Math.floor((millis /60000) %60),
            secs: Math.floor((millis /1000) %60),
            millis: millis
        }
}
function calculateUpdate(timestamp) {
    if (currentTetrimino) {
        let moved = fall();
        if (lastX === currentTetrimino.x && lastY === currentTetrimino.y && timestamp - lastMove >= 500) {
            if (!moved) {
                countingField.addTetrimino(currentTetrimino);
                currentTetrimino = undefined;
                dropping = false;
            }
        }
    }

    if (!currentTetrimino) {
        currentTetrimino = nextTetriminos.shift();
        nextTetriminos.push(generateTetrimino());
    }

    countedTPS = (1000 / (timestamp - lastUpdate)).toFixed(1);
    lastUpdate = Date.now();
}

function update() {
    let timestamp = Date.now();
    let time = millisConverter(timestamp - startTime);
    scoreTime.textContent = Basics.pad(time.hours, 2) +":"+ Basics.pad(time.mins, 2) +"."+ Basics.pad(time.secs, 2);

    if (timestamp - lastUpdate >= fallSpeed && !dropping) {
        calculateUpdate(timestamp);
    } else if(dropping) {
        if (timestamp - lastUpdate >= fallSpeed / 20) {
            calculateUpdate(timestamp);
        }
    }
}

function draw() {
    let timestamp = Date.now();

    scoreDisplay.textContent = countingField.updateScores();
    scoreLines.textContent = countingField.lines;
    scoreLevel.textContent = countingField.level;
    scoreGoal.textContent = countingField.goal;
    scoreTetrises.textContent = countingField.tetrises;
    scoreLpm.textContent = countingField.lpm.toFixed(3);
    scoreTpm.textContent = countingField.tpm.toFixed(3);
    fallSpeed = Math.pow((0.8 -((countingField.level -1) *0.007)),(countingField.level -1))*1000;

    mainWebgl.cls();
    mainRenderer.drawField(countingField, 10, 40);
    mainRenderer.drawTetrimino(currentTetrimino);

    holdWebgl.cls();
    holdRenderer.drawTetriminoAt(currentTetrimino, 1, 1);

    nextWebgl.cls();
    nextRenderer.drawTetriminoAt(nextTetriminos[0], 1, 1);
    nextRenderer.drawTetriminoAt(nextTetriminos[1], 1, 5);
    nextRenderer.drawTetriminoAt(nextTetriminos[2], 1, 9);
    nextRenderer.drawTetriminoAt(nextTetriminos[3], 1, 13);
    
    countedFPS = (1000 / (timestamp - lastDraw)).toFixed(1);
    lastDraw = Date.now();
}

function quantize(value, quant) {
    return Math.floor(value - (value % quant));
}

function aspectRatioWidth(height, aspectRatio = CFG.ASPECT_RATIO) {
    return Math.floor(height * aspectRatio);
}

function setDOMCanvasDimensions(canvas, width, height) {
    canvas.width = width;
    canvas.height = height;
    canvas.style.width = `${width}px`;
    canvas.style.height = `${height}px`;

    console.log(`[${CFG.LOGLEVEL.INFO}]${CFG.APPNAME}/setCanvasDimensions: `
        + `I set #${canvas.id}'s dimensions to: ${width}x${height}`);
}

function setLasts (){
    lastX = currentTetrimino.x;
    lastY = currentTetrimino.y;
    lastMove = Date.now();
}

function fall() {
    currentTetrimino.y += +1;

    if (!countingField.checkMove(currentTetrimino)) {
        currentTetrimino.y += -1;

        return false;
    } else {
        setLasts();

        return true;
    }
}

function moveLeft() {
    currentTetrimino.x += -1;

    if (!countingField.checkMove(currentTetrimino)) {
        currentTetrimino.x += +1;

        return false;
    } else {
        setLasts();

        return true;
    }
}

function moveRight() {
    currentTetrimino.x += +1;

    if (!countingField.checkMove(currentTetrimino)) {
        currentTetrimino.x += -1;

        return false;
    } else {
        setLasts();

        return true;
    }
}

function rotateRight() {
    currentTetrimino.rotate("right");

    if (!countingField.checkMove(currentTetrimino)) {
        currentTetrimino.rotate("left");

        return false;
    } else {
        setLasts();

        return true;
    }
}

function rotateLeft() {
    currentTetrimino.rotate("left");

    if (!countingField.checkMove(currentTetrimino)) {
        currentTetrimino.rotate("right");

        return false;
    } else {
        setLasts();

        return true;
    }
}

function togglePause() {
    gameRunning = !gameRunning;
    if (gameRunning) {
        document.querySelector("#pause-screen").remove();
        pageTitle.after(pauseButtonContent);
    }
    else {
        document.body.appendChild(pauseScreenContent);
        document.querySelector("#pause-button").remove();
        
    }
} window.togglePause = togglePause;

function keyDownHandler() {
    switch (event.key) {
        case "ArrowLeft": // LEFT
            if(gameRunning) moveLeft();
            break;

        case "ArrowUp": // UP
            if(gameRunning) rotateRight();
            break;

        case "ArrowRight": // RIGHT
            if(gameRunning) moveRight();
            break;

        case "ArrowDown": // DOWN
            if(gameRunning) dropping = true;
            break;
        
        case "Escape":
            togglePause();
            break;
        
        default:
            break;
    }
    clearEvent();
}

function keyUpHandler() {
    switch (event.key) {
        case "ArrowDown":
            dropping = false;
            break;
        default:
            break;
    }

    clearEvent();
}

function touchStartHandler() {
    if(gameRunning) {
        if (event.changedTouches) {
            touchStartPosition = {
                x: event.changedTouches[0].pageX,
                y: event.changedTouches[0].pageY
            }
        } else {
            touchStartPosition = {
                x: event.clientX,
                y: event.clientY
            }
        }
    }
    clearEvent();
}

function touchEndHandler() {
    if(gameRunning) {
        let proccessed = false;
        let touchPos;

        if (event.changedTouches) {
            touchPos = {
                x: event.changedTouches[0].pageX,
                y: event.changedTouches[0].pageY
            }
        } else {
            touchPos = {
                x: event.clientX,
                y: event.clientY
            }
        }

        let pageWidth = Basics.getPageWidth();
        if (touchPos.y > touchStartPosition.y + 150 && !proccessed) {
            dropping = true;
            proccessed = true;
        }

        if (touchPos.y < touchStartPosition.y - 150 && !proccessed) {
            rotateRight();
            proccessed = true;
        }

        if (touchPos.x > pageWidth / 2 && !proccessed) {
            moveRight();
            proccessed = true;
        }

        if (touchPos.x < pageWidth / 2 && !proccessed) {
            moveLeft();
            proccessed = true;
        }
    }
    clearEvent();
}

document.addEventListener("keydown", e => {
    event = e;

    requestAnimationFrame(keyDownHandler);
});

document.addEventListener("keyup", e => {
    event = e;
    requestAnimationFrame(keyUpHandler);
});

document.addEventListener("touchstart", e => {
    event = e;
    requestAnimationFrame(touchStartHandler);
});

document.addEventListener("touchend", e => {
    event = e;
    requestAnimationFrame(touchEndHandler);
});

function clearEvent() {
    event = {};
}

function loseGlContexts() {
    mainGl.getExtension('WEBGL_lose_context').loseContext();
    nextGl.getExtension('WEBGL_lose_context').loseContext();
    holdGl.getExtension('WEBGL_lose_context').loseContext();
}

window.addEventListener("beforeunload", loseGlContexts);

main();