import * as CFG from "../../Cfg.js";

export class Tetris {
    startTime = Date.now();

    score = 0
    lines = 9;
    level = 1;
    tetrises = 0;
    tpm = 0;
    lpm = 0;
    goal = 10;

    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.field = Tetris.makeField(width, height);
    }

    static makeField(width, height) {
        let temp = [];

        for (let i = 0; i < width * height; i++) {
            temp.push({
                tetrimino: undefined,
                empty: true
            });
        }
        return temp;
    }

    checkCollision(tetrimino) {
        for (var i = 0; i < tetrimino.width; i++) {
            for (var j = 0; j < tetrimino.height; j++) {
                if (tetrimino.model[tetrimino.rotation][tetrimino.width * j + i] > 0) {
                    let x = (tetrimino.x + i);
                    let y = (tetrimino.y + j);
                    if (!this.field[this.width * y + x].empty) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    checkBounding(tetrimino) {
        let model = tetrimino.model[tetrimino.rotation];

        for (let i = 0; i < tetrimino.height; i++) {
            for (let j = 0; j < tetrimino.width; j++) {

                let element = model[tetrimino.width * i + j];

                if (element > 0) {
                    if (tetrimino.x + j + 1 > this.width) {
                        console.log("[WARNING]JSTris/checkMove: out of bounds+r");
                        return false;
                    }
                    if (tetrimino.y + i + 1 > this.height) {
                        console.log("[WARNING]JSTris/checkMove: out of bounds+b");
                        return false;
                    }
                    if (tetrimino.x + j < 0 || tetrimino.y + i < 0) {
                        console.log("[WARNING]JSTris/checkMove: out of bounds-")
                        return false;
                    }
                }
            }
        }

        return true;
    }

    checkMove(tetrimino) {
        // console.log(`[${CFG.LOGLEVEL.INFO}]${CFG.APPNAME}/Tetris: checkMove. Field: `, this.field);
        if (!this.checkBounding(tetrimino))
            return false;
        if (!this.checkCollision(tetrimino))
            return false;

        return true;
    }

    addTetrimino(tetrimino) {
        console.log("[INFO]JSTris/addTetrimino: ", tetrimino);

        let model = tetrimino.model[tetrimino.rotation];

        for (let i = 0; i < tetrimino.height; i++) {
            for (let j = 0; j < tetrimino.width; j++) {

                let element = model[tetrimino.width * i + j];

                if (element > 0) {
                    this.field[this.width * (tetrimino.y + i) + (tetrimino.x + j)] = {
                        tetrimino: tetrimino.name,
                        empty: false
                    }
                }
            }
        }
        // console.log(`[INFO]JSTris/Tetris.field: `, this.field);
    }

    deleteRow(row) {
        let fieldWidth = this.width;
        for (let index = 1; index <= fieldWidth; index++) {
            this.field[fieldWidth * row + index - fieldWidth - 1] =
                {
                    tetrimino: undefined,
                    empty: true
                };
        }

        for(row = row; row > 0; row--) {
            for(let index = 1; index <= fieldWidth; index++) {
                this.field[fieldWidth * row + index - fieldWidth - 1] = row > 1 ?
                    this.field[fieldWidth * (row -1) + index - fieldWidth - 1] :
                        {
                            tetrimino: undefined,
                                empty: true
                        };
            }
        }
    }

    updateScores() {
        let rows = [];
        for (let row = this.height; row > 0; row--) {
            let counter = 0;
            for (let column = this.width; column > 0; column--) {
                let index = this.width * row + column - this.width - 1;
                if (!this.field[index].empty) {
                    counter++;
                }
            }
            if (counter === this.width) {
                rows.push(row);
            }
        }

        for (let i = 0; i < rows.length; i++) {
            this.deleteRow(rows[i]);
        }

        let count = rows.length;

        switch (count) {
            case 1:
                this.addToLines(1);
                return this.addToScore(100 * this.level);
            case 2:
                this.addToLines(2);
                return this.addToScore(300 * this.level);
            case 3:
                this.addToLines(3);
                return this.addToScore(500 * this.level);
            case 4:
                this.addToLines(4);
                this.addToTetrises(1);
                return this.addToScore(800 * this.level);
            default:
                return this.addToScore(0);
        }
    }
    
    addToLines(amount) {
        this.lines += amount;
        this.lpm = this.lines / this.getMinutesElapsed();

        if (this.lines >= (this.level - 1) * 5 + 10) {
            this.level++;
            this.goal = (this.level - 1) * 5 + 10;
        }

        return this.lines;        
    }

    getMinutesElapsed() {
        return (Date.now() - this.startTime) / 60000;
    }

    addToTetrises(amount) {
        this.tetrises += amount;
        this.tpm = this.tetrises / this.getMinutesElapsed();
        return this.tetrises;
    }

    addToScore(amount) {
        this.score += amount;
        return this.score;
    }

    cls() {
        this.field = Tetris.makeField(this.width, this.height);
    }
}