import { Tetriminos } from "./Tetriminos.js";

export class Tetrimino{
    constructor(type){
        switch(type){
            case "I":
                this.tetrimino = Tetriminos.I;
                break;
            case "O":
                this.tetrimino = Tetriminos.O;
                break;            
            case "T":
                this.tetrimino = Tetriminos.T;
                break;
            case "S":
                this.tetrimino = Tetriminos.S;
                break;
            case "Z":
                this.tetrimino = Tetriminos.Z;
                break;
            case "J":
                this.tetrimino = Tetriminos.J;
                break;           
            case "L":            
                this.tetrimino = Tetriminos.L;
                break;           
            default:
                break;
        }
        this.x = this.tetrimino.x;
        this.y = this.tetrimino.y;
        this.width = this.tetrimino.width;
        this.height = this.tetrimino.height;
        this.color = this.tetrimino.color;
        this.model = this.tetrimino.model;
        this.rotation = this.tetrimino.rotation;
        this.name = this.tetrimino.name;
        delete(this.tetrimino);
    }

    rotate(direction) {
        let clockwiseMatrix = ["N", "E", "S", "W"];
        let cClockwiseMatrix = ["N", "W", "S", "E"];
        switch (direction) {
            case "right":
                var current = clockwiseMatrix.indexOf(this.rotation);
                this.rotation = clockwiseMatrix[(current +1) % clockwiseMatrix.length];
                break;
            case "left":
                var current = cClockwiseMatrix.indexOf(this.rotation);
                this.rotation = cClockwiseMatrix[(current +1) % cClockwiseMatrix.length] ;
                break;
        }
    }

}