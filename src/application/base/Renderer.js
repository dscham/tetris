import {Tetriminos} from "./objects/Tetriminos.js";

export class Renderer {
    constructor(webgl, minoSize) {
        this.webgl = webgl;
        this.minoSize = minoSize;
    }

    static getInstance(webgl, minoSize) {
        return new Renderer(webgl, minoSize);
    }

    drawField(Field, width, height) {
//     let viewHeight = Field.height / height;
//     let field = Field.field.slice((Field.field.length / viewHeight), Field.field.length);
        let field = Field.field;
        for(var i = 0; i < width; i++) {
            for(var j = 0; j < height; j++) {
                let square = field[width * j + i];
                if(!square.empty) {
                    let x = i * this.minoSize;
                    let y = (j -20) * this.minoSize;
                    let color = Tetriminos[square.tetrimino].color;
                    this.webgl.drawRect(x, y, this.minoSize, this.minoSize, color);
                }
            }
        }
    }

    drawTetrimino(tetrimino) {
        for(var i = 0; i < tetrimino.width; i++) {
            for(var j = 0; j < tetrimino.height; j++) {
                if(tetrimino.model[tetrimino.rotation][tetrimino.width *j +i] > 0) {
                    let x = (tetrimino.x +i) *this.minoSize;
                    let y = (tetrimino.y +j -20) *this.minoSize;
                    let color = tetrimino.color;
                    this.webgl.drawRect(x, y, this.minoSize, this.minoSize, color);
                }
            }
        }
    }

    drawTetriminoAt(tetrimino, left, top) {
        for(var i = 0; i < tetrimino.width; i++) {
            for(var j = 0; j < tetrimino.height; j++) {
                if(tetrimino.model[tetrimino.rotation][tetrimino.width *j +i] > 0) {
                    let x = (left +i) *this.minoSize;
                    let y = (top +j) *this.minoSize;
                    let color = tetrimino.color;
                    this.webgl.drawRect(x, y, this.minoSize, this.minoSize, color);
                }
            }
        }
    }
}