export class WebGL{
    static colorConversion = 1.0 / 255;

    constructor(glctx, vertexShaderSource, fragmentShaderSource, gameCanvasWidth, gameCanvasHeight) {
        this.viewportWidth = gameCanvasWidth;
        this.viewportHeight = gameCanvasHeight;
        this.background = {
            "R" : 0.1,
            "G" : 0.1,
            "B" : 0.1,
            "A" : 1.0
        };
        this.color = {
            "R" : 1.0,
            "G" : 1.0,
            "B" : 1.0,
            "A" : 1.0           
        };

        if (!WebGL.vertexShaderSource || !WebGL.fragmentShaderSource) {
            WebGL.vertexShaderSource = vertexShaderSource;
            WebGL.fragmentShaderSource = fragmentShaderSource;
        }
        
        this.gl = glctx;
        this.gl.viewport(0, 0, this.viewportWidth, this.viewportHeight);
        this.vertexShader = WebGL.createShader(this.gl, this.gl.VERTEX_SHADER, WebGL.vertexShaderSource);
        this.fragmentShader = WebGL.createShader(this.gl, this.gl.FRAGMENT_SHADER, WebGL.fragmentShaderSource);
        this.program = WebGL.createProgram(this.gl, this.vertexShader, this.fragmentShader);
        this.gl.useProgram(this.program);
        this.resolutionUniformLocation = this.gl.getUniformLocation(this.program, "u_resolution");
        this.colorUniformLocation = this.gl.getUniformLocation(this.program, "u_color");
        this.positionAttributeLocation = this.gl.getAttribLocation(this.program, "a_position");
    }

    static getInstance(glctx, vertexShaderSource, fragmentShaderSource, gameCanvasWidth, gameCanvasHeight) {
        return new WebGL(glctx, vertexShaderSource, fragmentShaderSource, gameCanvasWidth, gameCanvasHeight);
    }

    static createShader(gl, type, source) {
        const shader = gl.createShader(type);
        gl.shaderSource(shader, source);
        gl.compileShader(shader);
        const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
        if (success) {
          return shader;
        }
       
        console.log(gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
    }

    static createProgram(gl, vertexShader, fragmentShader) {
        const program = gl.createProgram();
        gl.attachShader(program, vertexShader);
        gl.attachShader(program, fragmentShader);
        gl.linkProgram(program);
        const success = gl.getProgramParameter(program, gl.LINK_STATUS);
        if (success) {
          return program;
        }

        console.log(gl.getProgramInfoLog(program));
        gl.deleteProgram(program);
    }

    drawRect(x, y, width, height, color = this.color) {
        this.updateResolutionUniform();
    
        const flattenedVertices = this.getFlatRectVerticesForTriangleStrip(x, y, width, height);
        this.bufferFlatVertices(flattenedVertices);
        
        this.drawVerticesOfTypeWithColor(this.gl.TRIANGLE_STRIP, color, flattenedVertices.length);
    }

    updateResolutionUniform() { // Set shader uniform for resolution to clipspace conversion
        const resolution = [this.viewportWidth, this.viewportHeight];
        this.gl.uniform2fv(this.resolutionUniformLocation, resolution); 
    }

    getFlatRectVerticesForTriangleStrip(x, y, width, height) {
        const flatVertices = []; 

        flatVertices.push(x); flatVertices.push(y); // bottom left
        flatVertices.push(x); flatVertices.push(y + height); // top left
        flatVertices.push(x + width); flatVertices.push(y); // bottom right
        flatVertices.push(x + width); flatVertices.push(y + height); // top right

        return flatVertices;
    }

    bufferFlatVertices(flatVertices) {
        const positionBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, positionBuffer);

        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(flatVertices), this.gl.STATIC_DRAW);
        this.gl.enableVertexAttribArray(this.positionAttributeLocation);
    }

    drawVerticesOfTypeWithColor(drawType, color, count) {
        this.setColorUniform(color);
        this.drawVerticesOfType(drawType, count);
    }

    setColorUniform(color) {
        color = new Array(
            color.R * WebGL.colorConversion,
            color.G * WebGL.colorConversion,
            color.B * WebGL.colorConversion,
            color.A * WebGL.colorConversion
        );
        this.gl.uniform4fv(this.colorUniformLocation, color);
    }

    drawVerticesOfType(drawType, count) {
        const vertexDimensions = 2;                         // Tell the shader what kind of vertices we want to draw
        const vertexType = this.gl.FLOAT;
        const normalize = false;
        const stride = 0;
        const offset = 0;
        this.gl.vertexAttribPointer(this.positionAttributeLocation, vertexDimensions, vertexType, normalize, stride, offset);
    
        const vertexCount = count / vertexDimensions;       // Draw vertices
        this.gl.drawArrays(drawType, offset, vertexCount);
    }

    drawVertices(vertices, color = this.color) {
        this.updateResolutionUniform();
    
        const flattenedVertices = this.flattenVertices(vertices);
        this.bufferFlatVertices(flattenedVertices);
        
        this.drawVerticesOfTypeWithColor(this.gl.TRIANGLES, color, flattenedVertices.length);        
    }

    flattenVertices(vertices) {
        const flattenedVertices = [];

        vertices.array.forEach(vertex => {
            flattenedVertices.push(vertex.x);
            flattenedVertices.push(vertex.y);
        });

        return flattenedVertices;
    }

    cls(){
        this.gl.clearColor(this.background.R, this.background.G, this.background.B, this.background.A);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    }
} 