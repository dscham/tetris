export const APPNAME = "JSTris";
export const ASPECT_RATIO = 1/2;
export const FIELD_WIDTH = 10;
export const FIELD_HEIGHT = 40;

export const LOGLEVEL = {
     INFO : "INFO",
     WARNING : "WARNING",
     ERROR : "ERROR"
 }
